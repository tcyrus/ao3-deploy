# encoding: utf-8

import os
import re

from bs4 import BeautifulSoup, SoupStrainer
import requests

from .exceptions import LoginRequired, SessionExpired, UnexpectedError, ValidationError


AO3_URL = os.environ.get('AO3_URL', 'https://archiveofourown.org/')

if not AO3_URL.endswith('/'):
    AO3_URL += '/'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0'  # noqa: E501

REQUEST_HEADERS = {
    'User-Agent': USER_AGENT,
}

LOGIN_URL = f'{AO3_URL}users/login'
LOGOUT_URL = f'{AO3_URL}users/logout'

# URL you are redirected to if session expires
AUTH_ERROR_URL = f'{AO3_URL}auth_error'
# URL you are redirected to if cookie is "lost"
LOST_COOKIE_URL = f'{AO3_URL}lost_cookie'


def _validate_response_url(response):
    if response.status_code == 302:
        url = response.headers['Location']
    else:
        url = response.url

    if url == LOGIN_URL:
        raise LoginRequired

    expired_session_urls = (
        AUTH_ERROR_URL,
        LOST_COOKIE_URL,
    )
    if url in expired_session_urls:
        raise SessionExpired


def get_authenticity_token(text, form_id):
    strainer = SoupStrainer(id=form_id)
    soup = BeautifulSoup(text, 'lxml', parse_only=strainer)
    return soup.find(attrs={'name': 'authenticity_token'})['value']


def get_languages(text):
    strainer = SoupStrainer(id='work_language_id')
    soup = BeautifulSoup(text, 'lxml', parse_only=strainer)
    options = soup.find_all('option', value=True)
    return {
        option.string: option['value']
        for option in options
        if option['value']
    }


def get_validation_errors(text):
    errors = []

    soup = BeautifulSoup(text, 'lxml')
    error = soup.find(id='error')
    if error:
        errors += [li.text for li in error.find_all('li')]

    new_work_form = soup.find('form', id='new_work')
    if new_work_form:
        invalid_pseuds = new_work_form.find('h4', text=re.compile(r'These pseuds are invalid:'))
        if invalid_pseuds:
            errors += ['Invalid pseuds listed as authors']

    return errors


def _is_failed_login(response):
    return "The password or user name you entered doesn't match our records." in response.text


def login(username, password):
    # returns a logged-in ao3 session id or None if login failed.
    session = requests.Session()
    session.headers.update(REQUEST_HEADERS)

    # First, get an authenticity token.
    response = session.get(LOGIN_URL)
    authenticity_token = get_authenticity_token(response.text, 'loginform')

    # Now log in.
    login_data = {
        'utf8': '\u2713',
        'authenticity_token': authenticity_token,
        'user[login]': username,
        'user[password]': password,
        'commit': 'Log In',
    }

    response = session.post(
        LOGIN_URL,
        login_data,
        allow_redirects=True,
    )

    if _is_failed_login(response):
        return None

    return session


def logout(session):
    session.get(LOGOUT_URL)


def update_post(session, story_id, data):
    # Takes data, posts to ao3, and returns the URL for the created work
    # or raises an exception with validation errors.

    # First, get an authenticity token.
    response = session.get(
        f'{AO3_URL}works/{story_id}/edit',
        allow_redirects=False,
    )

    _validate_response_url(response)
    authenticity_token = get_authenticity_token(response.text, 'work-form')

    post_data = [
        ('utf8', '\u2713'),
        ('_method', 'patch'),
        ('authenticity_token', authenticity_token),
        ('post_button', 'Post'),
    ]

    post_data += data

    response = session.post(
        f'{AO3_URL}works/{story_id}',
        post_data,
        allow_redirects=True,
    )

    if response.status_code == 500:
        raise UnexpectedError('Received server error')

    _validate_response_url(response)

    if response.url == f'{AO3_URL}works/{story_id}/edit':
        validation_errors = get_validation_errors(response.content)
        if validation_errors:
            raise ValidationError(validation_errors)

    if response.status_code == 302:
        return response.headers['Location']

    return response.url
