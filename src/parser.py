import markdown

# Obtained from otwarchive source
# https://github.com/otwcode/otwarchive/blob/master/test/fixtures/locales.yml
# Commit: a18a4302b6f9e0a95f53dd0c2e7a54f05f4acedc

lang_ids = {
    'en': 1,
    'fr-FR': 2,
    'es-ES': 3,
    'de-DE': 4,
    'it-IT': 5,
    'ja-JP': 6,
    'cs-CZ': 7,
    'ru-RU': 8,
    'fi-FI': 9,
    'id-ID': 10,
    'nl-NL': 11,
    'pt-BR': 12,
    'zh-CHS': 13,
    'en-UK': 14
}


def process_file(filename):
    md = markdown.Markdown(extensions=['full_yaml_metadata'])
    with open(filename, 'r', encoding='utf-8') as f:
        content = md.convert(f.read())
    story_id = md.Meta.get('ao3_id')
    return (story_id, build_post_data(content, md.Meta))


def build_post_data(content, meta):
    post_data = [
        ('work[chapter_attributes][content]', content)
    ]

    rating = meta.get('ao3_rating')
    if rating is not None:
        post_data.append(
            ('work[rating_string]', rating)
        )

    archive_warnings = meta.get('ao3_archive_warnings')
    if archive_warnings is not None:
        post_data.extend(map(
            lambda warn: ('work[archive_warning_strings][]', warn),
            archive_warnings 
        ))

    fandoms = meta.get('ao3_fandoms')
    if fandoms is not None:
        post_data.append(
            ('work[fandom_string]', ', '.join(fandoms))
        )

    relationships = meta.get('ao3_relationships')
    if relationships is not None:
        post_data.append(
            ('work[relationship_string]', ', '.join(relationships))
        )

    characters = meta.get('ao3_characters')
    if characters is not None:
        post_data.append(
            ('work[character_string]', ', '.join(characters))
        )

    freeform_tags = meta.get('ao3_freeform_tags')
    if freeform_tags is not None:
        post_data.append(
            ('work[freeform_string]', ', '.join(freeform_tags))
        )

    title = meta.get('ao3_title')
    if title is not None:
        post_data.append(
            ('work[title]', title)
        )

    lang = meta.get('ao3_lang')
    if lang is not None:
        post_data.append(
            ('work[language_id]', lang_ids[lang])
        )

    author_ids = meta.get('ao3_author_ids')
    if author_ids is not None:
        post_data.extend(map(
            lambda auth_id: ('work[author_attributes][ids][]', auth_id),
            author_ids 
        ))

    restricted = meta.get('ao3_restricted')
    if restricted is not None:
        restricted = 1 if restricted else 0
        post_data.append(
            ('work[restricted]', restricted)
        )

    anon_commenting = meta.get('ao3_anon_commenting')
    if anon_commenting is not None:
        anon_commenting = 0 if anon_commenting else 1
        post_data.append(
            ('work[anon_commenting_disabled]', anon_commenting)
        )

    moderated_commenting = meta.get('ao3_moderated_commenting')
    if moderated_commenting is not None:
        moderated_commenting = 1 if moderated_commenting else 0
        post_data.append(
            ('work[moderated_commenting_enabled]', moderated_commenting)
        )

    return post_data
