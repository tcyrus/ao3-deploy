import os
import src.ao3 as ao3
import src.parser as parser

def main():
    sess = ao3.login(
        username=os.environ.get('AO3_USERNAME'),
        password=os.environ.get('AO3_PASSWORD')
    )

    for fname in os.listdir('posts'):
        if fname.endswith('.md'):
            story_id, data = parser.process_file(fname)
            ao3.update_post(sess, story_id, data)

if __name__ == '__main__':
    main()
